package com.gitlab.docserver.api.impl;

import com.gitlab.docserver.api.Artifact;
import com.gitlab.docserver.api.ArtifactVersion;
import com.gitlab.docserver.api.Group;
import lombok.Getter;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Getter
class GroupImpl implements Group {
    private final String groupId;

    private final Map<String, Artifact> artifactMap = new ConcurrentHashMap<>();

    public GroupImpl(String groupId) {
        Assert.hasText(groupId);

        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupImpl)) return false;

        GroupImpl group = (GroupImpl) o;

        return groupId.equals(group.groupId);

    }

    @Override
    public int hashCode() {
        return groupId.hashCode();
    }

    @Override
    public Artifact getArtifact(String artifactId) {
        if(!artifactMap.containsKey(artifactId)) {
            artifactMap.put(artifactId, new ArtifactImpl(this, artifactId));
        }

        return artifactMap.get(artifactId);
    }

    @Override
    public Collection<Artifact> getArtifacts() {
        return artifactMap.values().parallelStream().sorted(ArtifactImpl::compareTo).collect(Collectors.toList());
    }

    public void removeVersion(ArtifactVersion artifactVersion) {
        ArtifactImpl artifact = (ArtifactImpl)artifactVersion.getArtifact();
        artifact.removeVersion(artifactVersion);
        if(artifact.getVersions().isEmpty()) {
            artifactMap.remove(artifact.getArtifactId());
        }
    }

    public static int compareTo(Group a, Group b)
    {
        return a.getGroupId().compareTo(b.getGroupId());
    }
}
