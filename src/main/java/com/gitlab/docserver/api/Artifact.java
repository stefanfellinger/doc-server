package com.gitlab.docserver.api;

import java.util.List;

/**
 * Defines an artifact
 */
public interface Artifact {
    ArtifactVersion getLatestReleaseVersion();

    /**
     *
     * @return all versions belonging to this artifact
     */
    List<ArtifactVersion> getVersions();

    /**
     *
     * @return artifacts group id
     */
    String getGroupId();

    /**
     *
     * @return artifacts id
     */
    String getArtifactId();

    /**
     *
     * @return artifacts {@link Group} reference
     */
    Group getGroup();
}
