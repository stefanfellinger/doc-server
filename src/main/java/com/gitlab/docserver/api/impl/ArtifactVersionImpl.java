package com.gitlab.docserver.api.impl;

import com.gitlab.docserver.api.Artifact;
import com.gitlab.docserver.api.ArtifactVersion;
import com.gitlab.docserver.api.Group;
import lombok.Getter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;


/**
 * Implements an {@link ArtifactVersion}
 */
@Getter
class ArtifactVersionImpl implements ArtifactVersion {
    private final File versionRoot;
    private final Artifact artifact;
    private final String version;
    private final String path;

    private static final Pattern RELEASE_VERSION_PATTERN = Pattern.compile("[1-9]{1}[0-9]*\\.[0-9]*\\.[0-9]*");

    public ArtifactVersionImpl(Artifact artifact, ArtifactFileInfo artifactFileInfo) {
        Assert.notNull(artifact);
        Assert.notNull(artifactFileInfo);

        this.versionRoot = artifactFileInfo.getFile().getParentFile();
        this.artifact = artifact;
        this.version = artifactFileInfo.getVersion();
        this.path = artifactFileInfo.getPath();
    }

    @Override
    public String getGroupId() {
        return artifact.getGroupId();
    }

    @Override
    public String getArtifactId() {
        return artifact.getArtifactId();
    }

    public static int compareTo(ArtifactVersion a, ArtifactVersion b) {
        String[] versionA = a.getVersion().split("-")[0].split("\\.");
        String[] versionB = b.getVersion().split("-")[0].split("\\.");

        // check parts of the version
        int result = 0;
        for(int i = 0; i < versionA.length; ++i) {
            Integer va = Integer.parseInt(versionA[i]);
            Integer vb = Integer.parseInt(versionB[i]);
            result = va.compareTo(vb);
            if(result != 0) {
                break;
            }
        }

        // check snapshot
        if(result == 0) {
            result = a.isRelease()?1:-1;
        }

        return result;
    }

    @Override
    public boolean isRelease() {
        return RELEASE_VERSION_PATTERN.matcher(getVersion()).matches();
    }

    @Override
    public String toString() {
        return ArtifactUtil.toString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArtifactVersionImpl)) return false;

        ArtifactVersionImpl that = (ArtifactVersionImpl) o;

        if (!artifact.equals(that.artifact)) return false;
        return version.equals(that.version);

    }

    @Override
    public int hashCode() {
        int result = artifact.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }

    @Override
    public Group getGroup() {
        return artifact.getGroup();
    }

    /**
     * TODO: expensive on heavy load (many file operations) - implement some caching
     *
     * @return first pdf file if found any
     */
    @Override
    public File getPdfFile() {
        File[] matchingFiles = versionRoot.listFiles((FilenameFilter) new SuffixFileFilter("pdf"));
        return matchingFiles.length == 0
                ? null
                : matchingFiles[0];
    }
}
