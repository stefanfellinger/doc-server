package com.gitlab.docserver;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationPropertiesTest {

    @Test
    public void testDocPath1() {
        ApplicationProperties underTest = new ApplicationProperties();
        underTest.setDocPath("/1/2/3");
        assertThat(underTest.getDocPath()).isEqualTo("/1/2/3/");
    }

    @Test
    public void testDocPath2() {
        ApplicationProperties underTest = new ApplicationProperties();
        underTest.setDocPath("/1/2/3/");
        assertThat(underTest.getDocPath()).isEqualTo("/1/2/3/");
    }

    @Test
    public void testDocPath3() {
        ApplicationProperties underTest = new ApplicationProperties();
        underTest.setDocPath("/1/2/3///");
        assertThat(underTest.getDocPath()).isEqualTo("/1/2/3/");
    }

    @Test
    public void testDocPath4() {
        ApplicationProperties underTest = new ApplicationProperties();
        underTest.setDocPath("/1//2/3");
        assertThat(underTest.getDocPath()).isEqualTo("/1/2/3/");
    }
}