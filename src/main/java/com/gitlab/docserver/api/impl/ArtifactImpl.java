package com.gitlab.docserver.api.impl;

import com.gitlab.docserver.api.Artifact;
import com.gitlab.docserver.api.ArtifactVersion;
import com.gitlab.docserver.api.Group;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Defines a documentation artifact model
 */
@Getter
@Setter
final class ArtifactImpl implements Artifact {
    private String artifactId;
    private Group group;

    private Set<ArtifactVersion> versions = Collections.synchronizedSet(new HashSet<>());

    public ArtifactImpl(GroupImpl group, String artifactId) {
        Assert.notNull(group);
        Assert.hasText(artifactId);

        this.group = group;
        this.artifactId = artifactId;
    }

    void setGroup(GroupImpl group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return ArtifactUtil.toString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArtifactImpl)) return false;

        ArtifactImpl artifact = (ArtifactImpl) o;

        if (!artifactId.equals(artifact.artifactId)) return false;
        return group.equals(artifact.group);

    }

    @Override
    public int hashCode() {
        int result = artifactId.hashCode();
        result = 31 * result + group.hashCode();
        return result;
    }

    @Override
    public ArtifactVersion getLatestReleaseVersion()
    {
        final Iterator<ArtifactVersion> versionIterator = getVersions().listIterator();
        ArtifactVersion result = null;
        while (versionIterator.hasNext()) {
            ArtifactVersion version = versionIterator.next();
            if(version.isRelease()) {
                result = version;
                break;
            }
        }
        return result;
    }

    @Override
    public List<ArtifactVersion> getVersions() {
        return versions.stream().sorted((v1, v2) ->  ArtifactVersionImpl.compareTo(v2, v1)).collect(Collectors.toList());
    }

    @Override
    public String getGroupId() {
        return group.getGroupId();
    }

    public void removeVersion(ArtifactVersion artifactVersion) {
        this.versions.remove(artifactVersion);
    }

    public void addVersion(ArtifactFileInfo artifactFileInfo) {
        versions.add(new ArtifactVersionImpl(this, artifactFileInfo));
    }

    public static int compareTo(Artifact artifact, Artifact b)
    {
        return artifact.getArtifactId().compareTo(b.getArtifactId());
    }
}
